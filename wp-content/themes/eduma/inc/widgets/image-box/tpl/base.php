<?php
$title = !empty( $instance['title'] ) ? $instance['title'] : '' ;
$link = !empty( $instance['link'] ) ? $instance['link'] : '' ;
$image = !empty( $instance['image'] ) ? $instance['image'] : '' ;
$layout = !empty( $instance['layout'] ) ? ' layout-' . $instance['layout'] : '' ;

?>
<?php if( $image ) { ?>
<div class="thim-image-box<?php echo esc_attr( $layout );?>">
    <a href="<?php esc_url( $link );?>"><?php echo thim_get_feature_image( $image );?></a>
    <div class="title">
        <h3><a href="<?php esc_url( $link );?>"><?php echo esc_html( $title );?></a></h3>
    </div>
</div>
<?php }?>
