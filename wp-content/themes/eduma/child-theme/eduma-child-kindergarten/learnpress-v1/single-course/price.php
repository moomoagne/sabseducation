<?php
/**
 * Template for displaying the price of a course
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $course;

$unit_price = get_post_meta( $course->id, 'thim_course_unit_price', true );

?>


<div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<?php if ( $course->is_free() ) : ?>
		<div class="value free-course" itemprop="price" content="<?php esc_attr_e( 'Free', 'eduma-child' ); ?>">
			<?php esc_html_e( 'Free', 'eduma-child' ); ?>
		</div>
	<?php else: $price = learn_press_format_price( $course->get_price(), true ); ?>
		<div class="value " itemprop="price" content="<?php echo esc_attr( $price ); ?>">
			<?php echo esc_html( $price ); ?>
		</div>
		<?php echo ( ! empty( $unit_price ) ) ? '<div class="unit-price">' . $unit_price . '</div>' : ''; ?>
	<?php endif; ?>

	<meta itemprop="priceCurrency" content="<?php echo esc_attr(learn_press_get_currency()); ?>" />

</div>