<?php

function thim_child_enqueue_styles() {
	if ( is_multisite() ) {
		wp_enqueue_style( 'thim-child-style', get_stylesheet_uri(), array(), THIM_THEME_VERSION );
	} else {
		wp_enqueue_style( 'thim-parent-style', get_template_directory_uri() . '/style.css', array(), THIM_THEME_VERSION );
	}

	wp_enqueue_script( 'thim_child_script', get_stylesheet_directory_uri() . '/js/child_script.js', array( 'jquery' ), THIM_THEME_VERSION );
}

add_action( 'wp_enqueue_scripts', 'thim_child_enqueue_styles', 100 );

function thim_child_enqueue_admin_script() {
	wp_dequeue_style( 'learn-press-mb-course' );
	wp_dequeue_script( 'learn-press-mb-course' );
}

add_action( 'admin_enqueue_scripts', 'thim_child_enqueue_admin_script', 1000 );

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
//Add meta-box-course.js into single course dashboard
if ( is_admin() && is_plugin_active( 'learnpress/learnpress.php' ) ) {
	global $current_screen;
	if ( $current_screen ) {
		$screen_id = $current_screen->id;
		if ( in_array( $screen_id, array( "edit-lp_course" ) ) ) {
			LP_Admin_Assets::enqueue_script( 'meta-box-course', learn_press_plugin_url( 'assets/js/admin/meta-box-course.js' ), array( 'jquery' ) );
		}
	}
}

load_theme_textdomain( 'eduma-child', get_stylesheet_directory() . '/languages' );

//Replace courses meta
function thim_add_course_meta( $meta_box ) {
	$fields             = array();
	$fields[]           = array(
		'name' => esc_html__( 'Duration Info', 'eduma-child' ),
		'id'   => 'thim_course_duration',
		'type' => 'text',
		'desc' => esc_html__( 'Display duration info', 'eduma-child' ),
		'std'  => esc_html__( '30 hours', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Class Size', 'eduma-child' ),
		'id'   => 'thim_course_class_size',
		'type' => 'number',
		'desc' => esc_html__( 'Class Size', 'eduma-child' ),
		'std'  => esc_html__( '30', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Available Seats', 'eduma-child' ),
		'id'   => 'thim_course_available_seats',
		'type' => 'number',
		'desc' => esc_html__( 'Enter available seats', 'eduma-child' ),
		'std'  => esc_html__( '10', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Years Old', 'eduma-child' ),
		'id'   => 'thim_course_year_old',
		'type' => 'text',
		'desc' => esc_html__( 'Enter age', 'eduma-child' ),
		'std'  => esc_html__( '2 - 4', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Price', 'eduma-child' ),
		'id'   => 'thim_course_price',
		'type' => 'text',
		'desc' => esc_html__( 'Enter course price', 'eduma-child' ),
		'std'  => esc_html__( '$50', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Unit', 'eduma-child' ),
		'id'   => 'thim_course_unit_price',
		'type' => 'text',
		'desc' => esc_html__( 'Enter unit, for example, p/h, person/hour', 'eduma-child' ),
		'std'  => esc_html__( 'p/h', 'eduma-child' )
	);
	$fields[]           = array(
		'name' => esc_html__( 'Media Intro', 'eduma' ),
		'id'   => 'thim_course_media_intro',
		'type' => 'textarea',
		'desc' => esc_html__( 'Enter media intro', 'eduma' ),
	);
	$meta_box['fields'] = $fields;

	return $meta_box;
}

//Override function thim_add_course_remove_meta on parent theme
function thim_add_course_remove_meta( $meta_box ) {
	$meta_box['fields'] = array();
	return $meta_box;
}

// remove curriculum
add_action( 'init', 'thim_remove_course_curriculum', 10 );
if ( !function_exists( 'thim_remove_course_curriculum' ) ) {
	function thim_remove_course_curriculum() {
		if ( class_exists( 'LP_Course_Post_Type' ) ) {

			remove_action( 'edit_form_after_editor', array( LP_Course_Post_Type::instance(), 'curriculum_editor' ), 0 );
		}
	}
}

add_filter( 'rwmb_show_course_assessment', '__return_false' );
add_filter( 'rwmb_show_course_curriculum', '__return_false' );
add_filter( 'rwmb_show_course_payment', '__return_false' );

//Remove tab assessment & payment from LP 2.1.3
add_filter( 'learn_press_lp_course_tabs', 'thim_remove_tabs_course' );
function thim_remove_tabs_course( $tabs ) {
	if ( !empty( $tabs ) ) {
		foreach ( $tabs as $tab_id => $tab ) {
			if ( !empty( $tab->meta_box ) && is_array( $tab->meta_box ) ) {
				$id = $tab->meta_box['id'];
				if ( !empty( $id ) ) {
					if ( in_array( $id, array( 'course_payment', 'course_assessment' ) ) ) {
						unset( $tabs[$tab_id] );
					}
				}
			}
		}
	}
	return $tabs;
}


function thim_meta_course_kindergarten( $course_id ) {

	$class_size = get_post_meta( $course_id, 'thim_course_class_size', true );
	$year_old   = get_post_meta( $course_id, 'thim_course_year_old', true );
	$price      = get_post_meta( $course_id, 'thim_course_price', true );
	$unit_price = get_post_meta( $course_id, 'thim_course_unit_price', true );
	?>
	<?php if ( !empty( $class_size ) ): ?>
		<div class="class-size">
			<label><?php esc_html_e( 'Class Size', 'eduma-child' ); ?></label>

			<div class="value"><?php echo esc_html( $class_size ); ?></div>
		</div>
	<?php endif; ?>

	<?php if ( !empty( $year_old ) ): ?>
		<div class="year-old">
			<label><?php esc_html_e( 'Years Old', 'eduma-child' ); ?></label>

			<div class="value"><?php echo esc_html( $year_old ); ?></div>
		</div>
	<?php endif; ?>

	<div class="course-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<div class="value " itemprop="price" content="<?php echo esc_attr( $price ); ?>">
			<?php echo esc_html( $price ); ?>
		</div>
		<?php echo ( !empty( $unit_price ) ) ? '<div class="unit-price">' . $unit_price . '</div>' : ''; ?>
	</div>

	<?php

}

function thim_related_courses() {
	$related_courses = thim_get_related_courses( 3 );

	if ( $related_courses ) {
		?>
		<div class="thim-ralated-course">
			<h3 class="related-title"><?php esc_html_e( 'You May Like', 'eduma-child' ); ?></h3>

			<div class="thim-course-grid">
				<?php foreach ( $related_courses as $course_item ) : ?>

					<article class="course-grid-3 lpr_course">
						<div class="course-item">
							<div class="course-thumbnail">
								<a href="<?php echo get_the_permalink( $course_item->ID ); ?>">
									<?php
									echo thim_get_feature_image( get_post_thumbnail_id( $course_item->ID ), 'full', 450, 450, get_the_title( $course_item->ID ) );
									?>
								</a>
								<?php do_action( 'thim_inner_thumbnail_course' ); ?>
								<?php echo '<a class="course-readmore" href="' . esc_url( get_the_permalink( $course_item->ID ) ) . '">' . esc_html__( 'Read More', 'eduma-child' ) . '</a>'; ?>
							</div>
							<div class="thim-course-content">

								<h4 class="course-title">
									<a href="<?php echo esc_url( get_the_permalink( $course_item->ID ) ); ?>"> <?php echo get_the_title( $course_item->ID ); ?></a>
								</h4>

								<div class="course-author">
									<?php echo get_avatar( $course_item->post_author, 40 ); ?>
									<div class="author-contain">
										<div class="value">
											<?php echo get_the_author_meta( 'display_name', $course_item->post_author ); ?>
										</div>
									</div>
								</div>

								<div class="thim-background-border"></div>

								<div class="course-meta">
									<?php thim_meta_course_kindergarten( $course_item->ID ); ?>
								</div>

							</div>
						</div>
					</article>
				<?php endforeach; ?>
			</div>
		</div>
		<?php
	}
}

function thim_course_info() {

	$course_id = get_the_ID();

	$duration        = get_post_meta( $course_id, 'thim_course_duration', true );
	$class_size      = get_post_meta( $course_id, 'thim_course_class_size', true );
	$year_old        = get_post_meta( $course_id, 'thim_course_year_old', true );
	$available_seats = get_post_meta( $course_id, 'thim_course_available_seats', true );
	$thim_options    = get_theme_mods();

	$category = wp_get_post_terms( $course_id, 'course_category' );

	$cat_name = $category[0]->name;

	?>
	<div class="thim-course-info">
		<h3 class="title"><?php esc_html_e( 'Course Features', 'eduma-child' ); ?></h3>
		<ul>
			<li>
				<i class="fa fa-clock-o"></i>
				<span class="label"><?php esc_html_e( 'Duration', 'eduma-child' ); ?></span>
				<span class="value"><?php echo esc_html( $duration ); ?></span>
			</li>
			<li>
				<i class="fa fa-futbol-o"></i>
				<span class="label"><?php esc_html_e( 'Activities', 'eduma-child' ); ?></span>
				<span class="value"><?php echo esc_html( $cat_name ); ?></span>

			</li>
			<li>
				<i class="fa fa-users"></i>
				<span class="label"><?php esc_html_e( 'Class Sizes', 'eduma-child' ); ?></span>
				<span class="value"><?php echo esc_html( $class_size ); ?></span>

			</li>
			<li>
				<i class="fa fa-sun-o"></i>
				<span class="label"><?php esc_html_e( 'Years Old', 'eduma-child' ); ?></span>
				<span class="value"><?php echo esc_html( $year_old ); ?></span>
			</li>
			<li>
				<i class="fa fa-user-plus"></i>
				<span class="label"><?php esc_html_e( 'Available Seats', 'eduma-child' ); ?></span>
				<span class="value"><?php echo esc_html( $available_seats ); ?></span>
			</li>

		</ul>
		<?php
		if ( !empty( $thim_options['thim_learnpress_timetable_link'] ) ) {
			echo '<div class="text-center"><a class="thim-timetable-link" target="_blank" href="' . esc_url( $thim_options['thim_learnpress_timetable_link'] ) . '">' . esc_html( 'Courses Schedules', 'eduma' ) . '</a></div>';
		}
		?>
	</div>
	<?php
}


function thim_tp_register_required_plugins() {
	$plugins = array(
		array(
			'name'     => 'SiteOrigin Page Builder',
			'slug'     => 'siteorigin-panels',
			'required' => true,
		),

		array(
			'name'     => 'Black Studio TinyMCE Widget',
			'slug'     => 'black-studio-tinymce-widget',
			'required' => false,
		),
		array(
			'name'     => 'Contact Form 7',
			'slug'     => 'contact-form-7',
			'required' => false
		),
		array(
			'name'     => 'MailChimp for WordPress',
			'slug'     => 'mailchimp-for-wp',
			'required' => false
		),
		array(
			'name'     => 'WooCommerce',
			'slug'     => 'woocommerce',
			'required' => false,
		),
		array(
			'name'     => 'Widget Logic',
			'slug'     => 'widget-logic',
			'required' => false,
		),
		array(
			'name'     => 'Social Login',
			'slug'     => 'miniorange-login-openid',
			'required' => false,
		),

		array(
			'name'               => 'Slider Revolution',
			// The plugin name
			'slug'               => 'revslider',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/revslider.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '5.2.5.3',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Thim Framework',
			// The plugin name
			'slug'               => 'thim-framework',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/thim-framework.zip',
			// The plugin source
			'required'           => true,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.9.5',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Thim Events',
			// The plugin name
			'slug'               => 'tp-event',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/tp-event.zip',
			// The plugin source
			'required'           => false,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.4.1.1',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Thim Portfolio',
			// The plugin name
			'slug'               => 'tp-portfolio',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/tp-portfolio.zip',
			// The plugin source
			'required'           => false,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.3',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'               => 'Thim Our Team',
			// The plugin name
			'slug'               => 'thim-our-team',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/thim-our-team.zip',
			// The plugin source
			'required'           => false,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.3.1',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),
		array(
			'name'               => 'Thim Testimonials',
			// The plugin name
			'slug'               => 'thim-testimonials',
			// The plugin slug (typically the folder name)
			'source'             => THIM_DIR . 'inc/plugins/thim-testimonials.zip',
			// The plugin source
			'required'           => false,
			// If false, the plugin is only 'recommended' instead of required
			'version'            => '1.3.1',
			// E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation'   => false,
			// If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false,
			// If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url'       => '',
			// If set, overrides default API URL and points to an external URL
		),

		array(
			'name'     => 'LearnPress',
			'slug'     => 'learnpress',
			'required' => true,
		),

	);

	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       => 'eduma', // Text domain - likely want to be the same as your theme.
		'default_path' => '', // Default absolute path to pre-packaged plugins
		'parent_slug'  => 'themes.php', // Default parent menu slug
		'menu'         => 'install-required-plugins', // Menu slug
		'has_notices'  => true, // Show admin notices or not
		'is_automatic' => false, // Automatically activate plugins after installation or not
		'message'      => '', // Message to output right before the plugins table
		'strings'      => array(
			'page_title'                      => esc_html__( 'Install Required Plugins', 'eduma' ),
			'menu_title'                      => esc_html__( 'Install Plugins', 'eduma' ),
			'installing'                      => esc_html__( 'Installing Plugin: %s', 'eduma' ),
			// %1$s = plugin name
			'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'eduma' ),
			'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'eduma' ),
			// %1$s = plugin name(s)
			'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'eduma' ),
			// %1$s = plugin name(s)
			'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'eduma' ),
			'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'eduma' ),
			'return'                          => esc_html__( 'Return to Required Plugins Installer', 'eduma' ),
			'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'eduma' ),
			'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'eduma' ),
			// %1$s = dashboard link
			'nag_type'                        => 'updated'
			// Determines admin notice type - can only be 'updated' or 'error'
		)
	);
	tgmpa( $plugins, $config );
}

function thim_child_manage_course_columns( $columns ) {
	unset( $columns['price'] );
	$keys   = array_keys( $columns );
	$values = array_values( $columns );
	$pos    = array_search( 'sections', $keys );
	if ( $pos !== false ) {
		array_splice( $keys, $pos + 1, 0, array( 'thim_price' ) );
		array_splice( $values, $pos + 1, 0, __( 'Price', 'eduma' ) );
		$columns = array_combine( $keys, $values );
	} else {
		$columns['thim_price'] = __( 'Price', 'eduma' );
	}
	return $columns;
}

add_filter( 'manage_lp_course_posts_columns', 'thim_child_manage_course_columns' );

function thim_child_manage_course_columns_content( $column ) {
	global $post;
	switch ( $column ) {
		case 'thim_price':
			$price      = get_post_meta( $post->ID, 'thim_course_price', true );
			$unit_price = get_post_meta( $post->ID, 'thim_course_unit_price', true );
			echo $price . ' ' . $unit_price;
	}
}

add_filter( 'manage_lp_course_posts_custom_column', 'thim_child_manage_course_columns_content' );
