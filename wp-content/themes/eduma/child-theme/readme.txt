/* EDUMA CHILD THEME 1.0.0 */

-- Notes --

Any questions about child theme, feel free to discuss with our support team at https://thimpress.com/forums/forum/eduma/.

Or email me at candy@thimpress.com with subject "Eduma Child Theme Support".

If you like the theme and our product please leave us a ★★★★★ rating at https://themeforest.net/downloads#item-14058034

A huge thank you from ThimPress Team in advance!


