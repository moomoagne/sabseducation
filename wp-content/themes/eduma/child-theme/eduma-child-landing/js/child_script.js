(function ($) {
	"use strict";

	var thim_round_images = function () {

		if( $(window).width() > 768 ) {
			$('.thim-round-images-slider').each(function () {
				var elem = $(this);
				$(this).thimContentSlider({
					items            : elem,
					itemsVisible     : 3,
					mouseWheel       : false,
					autoPlay         : false,
					itemMaxWidth     : 620,
					itemMinWidth     : 400,
					activeItemRatio  : 1.1111,
					activeItemPadding: -280,
					itemPadding      : 0
				});

			});
		}else{
			$('.thim-round-images-slider').each(function () {
				var elem = $(this);
				$(this).thimContentSlider({
					items            : elem,
					itemsVisible     : 3,
					mouseWheel       : false,
					autoPlay         : false,
					itemMaxWidth     : 620,
					itemMinWidth     : 200,
					activeItemRatio  : 1.1111,
					activeItemPadding: -150,
					itemPadding      : 0
				});

			});
		}
	};

	var thim_round_carousel = function () {
		var elem = $('.thim-cloud-carousel .content'),
			elemW = elem.width();
		$(".thim-cloud-carousel .content").CloudCarousel({
			minScale   : 0.6,
			xPos       : elemW/2,
			yPos       : 30,
			xRadius: elemW/3,
			yRadius: 30,
			buttonLeft : $(".thim-cloud-carousel .prev-text"),
			buttonRight: $(".thim-cloud-carousel .next-text"),
			altBox     : $(".thim-cloud-carousel .alt-text"),
			titleBox   : $(".thim-cloud-carousel .title-text"),
			bringToFront: true
		});
	};

	$(window).load(function(){

		$('.width-navigation .current_page_item>a,.thim-all-demo .widget-button').on('click', function(e) {
			e.preventDefault();
			var elem = $('.thim-landing-demo-all'),
				elem_top = elem.offset().top;
			if( elem.length > 0 ) {
				$('body').animate({
					scrollTop: elem_top
				}, 800);
			}
		});

		$(document).on('click', '.scrollable li:not(.mid-active)', function(e) {
			$('.thim-round-images-slider ul li').removeClass('mid-active');
			$(this).addClass('mid-active');
		});
	});


	$(document).ready(function () {
		thim_round_images();
		thim_round_carousel();


	});

})(jQuery);