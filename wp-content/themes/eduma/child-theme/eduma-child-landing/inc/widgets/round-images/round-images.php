<?php
if ( class_exists( 'Thim_Widget' ) ) {
	class Thim_Round_Images extends Thim_Widget {

		function __construct() {

			parent::__construct(
				'round-images',
				esc_html__( 'Thim: Round Images', 'eduma' ),
				array(
					'description'   => esc_html__( '', 'eduma' ),
					'help'          => '',
					'panels_groups' => array( 'thim_widget_group' ),
					'panels_icon'   => 'dashicons dashicons-welcome-learn-more'
				),
				array(),
				array(
					'image' => array(
						'type'        => 'multimedia',
						'label'       => esc_html__( 'Image', 'eduma' ),
						'description' => esc_html__( 'Select image from media library.', 'eduma' )
					),

				)
			);
		}

		function get_template_name( $instance ) {
			return 'base';
		}

		function get_style_name( $instance ) {
			return false;
		}
	}

	function thim_round_image_register_widget() {
		register_widget( 'Thim_Round_Images' );
	}

	add_action( 'widgets_init', 'thim_round_image_register_widget' );
}
