<?php

$number = 3;

if ( $instance['image'] ) {


	$img_id = explode( ",", $instance['image'] );

	echo '<div class="thim-round-images-slider">';
	$i = 0;
	foreach ( $img_id as $id ) {
		$src = wp_get_attachment_image_src( $id, 'full' );
		if ( $src ) {
			$img_size = '';
			$src_size = @getimagesize( $src['0'] );
			$image    = '<div class="image"><img src ="' . esc_url( $src['0'] ) . '" ' . $src_size[3] . ' alt=""/></div>';
		}
		echo '<div class="item" >'  . $image . "</div>";
		$i ++;
	}
	echo "</div>";
}