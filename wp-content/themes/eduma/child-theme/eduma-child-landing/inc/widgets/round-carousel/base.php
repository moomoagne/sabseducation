<?php

echo '<div class="thim-cloud-carousel">';
echo '<div class="content">';
if ( $instance['tab'] ) {
	foreach ( $instance['tab'] as $i => $tab ) {
		$img = wp_get_attachment_image_src( $tab['image'], 'full' );
		if( $img ) {
			$src_size = @getimagesize( $img['0'] );
			$image    = '<img class="cloudcarousel" src ="' . esc_url( $img['0'] ) . '" ' . $src_size[3] . ' alt="'.esc_attr($tab['title']).'" title="'.esc_attr($tab['content']).'"/>';
			echo $image;
		}
	}
}
echo '</div>';
echo '<div class="next-text fa fa-chevron-right"></div>';
echo '<div class="prev-text fa fa-chevron-left"></div>';
echo '<h4 class="alt-text"></h4>';
echo '<div class="title-text"></div>';
echo '</div>';
