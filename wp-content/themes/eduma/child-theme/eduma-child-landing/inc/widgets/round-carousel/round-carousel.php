<?php
if ( class_exists( 'Thim_Widget' ) ) {
	class Thim_Round_Carousel extends Thim_Widget {
		function __construct() {
			parent::__construct(
				'round-carousel',
				esc_html__( 'Thim: Round Carousel', 'eduma' ),
				array(
					'description'   => esc_html__( '', 'eduma' ),
					'help'          => '',
					'panels_groups' => array( 'thim_widget_group' ),
					'panels_icon'   => 'dashicons dashicons-welcome-learn-more'
				),
				array(),
				array(
					'tab' => array(
						'type'      => 'repeater',
						'label'     => esc_html__( 'Tab', 'eduma' ),
						'item_name' => esc_html__( 'Tab', 'eduma' ),
						'fields'    => array(
							'image'   => array(
								'type'        => 'media',
								'label'       => esc_html__( 'Image', 'eduma' ),
								'description' => esc_html__( 'Select image from media library.', 'eduma' )
							),
							'title'   => array(
								"type"                  => "text",
								"label"                 => esc_html__( "Tab Title", 'eduma' ),
								"default"               => esc_html__( "Tab Title", 'eduma' ),
								"allow_html_formatting" => array(
									'a'      => array(
										'href'   => true,
										'target' => true,
										'class'  => true,
										'alt'    => true,
										'title'  => true,
									),
									'br'     => array(),
									'em'     => array(),
									'strong' => array(),
									'span'   => array(),
									'i'      => array(
										'class' => true,
									),
									'b'      => array(),
								)
							),
							'content' => array(
								"type"                  => "textarea",
								"label"                 => esc_html__( "Content", 'eduma' ),
								"allow_html_formatting" => true
							),
						),
					),
				)
			);
		}

		function get_template_name( $instance ) {
			return 'base';
		}

		function get_style_name( $instance ) {
			return false;
		}
	}

	function thim_round_carousel_register_widget() {
		register_widget( 'Thim_Round_Carousel' );
	}

	add_action( 'widgets_init', 'thim_round_carousel_register_widget' );
}
